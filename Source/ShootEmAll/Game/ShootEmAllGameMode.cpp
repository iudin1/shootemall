// Copyright Epic Games, Inc. All Rights Reserved.

#include "ShootEmAllGameMode.h"
#include "ShootEmAllPlayerController.h"
#include "ShootEmAll/Character/ShootEmAllCharacter.h"
#include "UObject/ConstructorHelpers.h"

AShootEmAllGameMode::AShootEmAllGameMode()
{
	// use our custom PlayerController class
	PlayerControllerClass = AShootEmAllPlayerController::StaticClass();

	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/Blueprints/Character/TopDownCharacter"));
	if (PlayerPawnBPClass.Class != nullptr)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
	
}