// Copyright Epic Games, Inc. All Rights Reserved.

#include "ShootEmAll.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, ShootEmAll, "ShootEmAll" );

DEFINE_LOG_CATEGORY(LogShootEmAll)
 